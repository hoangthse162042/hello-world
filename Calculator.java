import java.util.Scanner;

public class Calculator {
  public static void main(String[] args) {

    char operator;
    Double number1, number2, result;
    number1 = 23.0;
    number2 = 56.0; 
    // create an object of Scanner class

    System.out.println("Simple Calculator: +, -, * and /");
    operator =  '+'; //input.next().charAt(0);

    // ask users to enter numbers
    System.out.println("First number is " + number1); //input.nextDouble();

    System.out.println("Second number is "+ number2);//input.nextDouble();


      // performs addition between numbers
        System.out.print("Addition: ");
        result = number1 + number2;
        System.out.println(number1 + " + " + number2 + " = " + result);
        System.out.println("");
      // performs subtraction between numbers
          System.out.print("Subtraction: ");
        result = number1 - number2;
        System.out.println(number1 + " - " + number2 + " = " + result);
        System.out.println("");
      // performs multiplication between numbers
        System.out.print("Multiplication:");
        result = number1 * number2;
        System.out.println(number1 + " * " + number2 + " = " + result);
        System.out.println("");
      // performs division between numbers
        System.out.print("Division: ");
        result = number1 / number2;
        System.out.println(number1 + " / " + number2 + " = " + result);

  }
}
